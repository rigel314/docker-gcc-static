FROM alpine:3.7

RUN apk update && apk add build-base cppcheck cppcheck-htmlreport clang clang-dev clang-analyzer git
