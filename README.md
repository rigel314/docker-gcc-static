docker-gcc-static
=======
# About
This really just exists so I can run cppcheck and scan-build during CI tests of my C programs.

# Usage
```yaml
jobName:
  tags:
    - linux
    - docker
  image: $CI_REGISTRY/rigel314/docker-gcc-static:latest
```
